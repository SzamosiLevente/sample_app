class MicropostsController < ApplicationController
  load_and_authorize_resource
  before_action :logged_in_user, only: [:create, :destroy,:update]
  # before_action :correct_user,   only: :destroy

  def index
        # @posts = Micropost.all
        @posts = Micropost.only_deleted.ransack(params[:q])
        @user_profiles = @posts.result.includes(:user)

        @all = @posts.result
        @deleted = @posts.result.where(id: 1)
        @active= @posts.result.where(id: 2)

  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end

  def update
    @micropost.update_attribute(micropost_params)
    flash[:success] = "Micropost updated"
    redirect_to request.referrer || root_url
  end

   def react
    if actual_reaction
      if actual_reaction.reaction_type == params[:r_typ]
        actual_reaction.destroy
      else
        actual_reaction.update_attribute(:reaction_type, params[:r_typ])
      end
    else
      Reaction.new(user_id: current_user.id,micropost_id: params[:id],reaction_type: params[:r_typ]).save
    end
    redirect_to current_user 
  end


  private

    def micropost_params
      params.require(:micropost).permit(:content,:visibility)
    end

    def actual_reaction
      Reaction.where("user_id = ? and micropost_id = ?", current_user.id,params[:id]).first
    end
    

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
