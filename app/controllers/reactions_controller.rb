class ReactionsController < ApplicationController

  def create
  	Reaction.create(reaction_params)
  	redirect_to root_url
  end

  def destroy
	reaction = Reaction.find(params[:id])
  	reaction.destroy
  	redirect_to root_url

  end

  def update
	reaction = Reaction.find(params[:id])
   	reaction.update_attributes(reaction_params)
	redirect_to root_url
  end

    private

    def reaction_params
      params.permit(:user_id,:micropost_id,:reaction_type)
    end

end
