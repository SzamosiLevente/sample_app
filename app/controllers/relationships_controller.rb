class RelationshipsController < ApplicationController
  before_action :logged_in_user
  before_action :user_right ,     only: :update

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def update
    @user = Relationship.find(params[:id]).followed
    rel = Relationship.find(params[:id])
    if(params[:todo])=="accept"
      rel.update_attribute(:pending, false)
    else
      rel.destroy
    end
    redirect_to @user 

  end

private

  def user_right
     @user = Relationship.find(params[:id]).followed
    redirect_to(root_url) unless current_user?(@user)
  end

end
