class StaticPagesController < ApplicationController
  def home
  	if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end

  end

  def help
  end

  def about
  end
  
  def contact
  end

  def set_languange_en

    I18n.locale = :en
  end

  def set_languange_hu
    I18n.locale =  :hu
  end

end
