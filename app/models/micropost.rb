class Micropost < ApplicationRecord
  acts_as_paranoid
  belongs_to :user

  has_many :reaction, dependent: :destroy

  enum visibility: { Public: 0, Private: 1}

  # default_scope -> { where(:deleted_at => nil) }
   	default_scope -> { order(created_at: :desc) }
  #mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
#  validate  :picture_size

#  private
#
#    def picture_size
#      if picture.size > 5.megabytes
#        errors.add(:picture, "should be less than 5MB")
#      end
#    end
end
