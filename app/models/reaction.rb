class Reaction < ApplicationRecord
	belongs_to :user
  	belongs_to :micropost
  	enum reaction_type: { Like: 1, Dislike: 2, "Don't care":3}
  	#{1=>"Like", 2=>"Dislike", 3=>"Don't care"}
end
