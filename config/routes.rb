Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'

get  '/help',    to: 'static_pages#help'
get  '/about',   to: 'static_pages#about'
get  '/contact', to: 'static_pages#contact'
get  '/signup',  to: 'users#new'
get    '/login',   to: 'sessions#new'
post   '/login',   to: 'sessions#create'
post   '/imitate',   to: 'sessions#update'
delete '/logout',  to: 'sessions#destroy'
post  '/set_languange_hu',    to: 'static_pages#set_languange_hu'
post  '/set_languange_en',    to: 'static_pages#set_languange_en'



resources :microposts do
    member do
      post :react
    end
  end

resources :users do
    member do
      get :following, :followers, :pending
    end
  end

resources :account_activations, only: [:edit]
resources :password_resets,     only: [:new, :create, :edit, :update]
resources :microposts,          only: [:create, :destroy]
resources :relationships,       only: [:create, :destroy, :update]
#resources :reactions,       	only: [:create, :destroy, :update]


end
