class AddVisibilityToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :visibility, :integer, default: 0
  end
end
