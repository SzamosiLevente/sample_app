class AddPendingToRelationships < ActiveRecord::Migration[5.0]
  def change
    add_column :relationships, :pending, :boolean
  end
end
