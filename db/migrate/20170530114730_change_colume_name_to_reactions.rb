class ChangeColumeNameToReactions < ActiveRecord::Migration[5.0]
  def change
  	rename_column :reactions, :reaction_type, :reaction_type
  end
end
