class AddIndexToReactions < ActiveRecord::Migration[5.0]
  def change
    add_index :reactions, [:user_id, :micropost_id], unique: true
  end
end
